public class ColorSort {

   enum Color {red, green, blue}

   public static void main(String[] param) {
      // for debugging
   }

   public static void reorder(Color[] balls) {

      int countRed = 0;
      int countGreen = 0;
      int countBlue = 0;

      for (int i = 0; i < balls.length; i++) {
         if (balls[i] == Color.red) {
            countRed++;
         }
         else if (balls[i] == Color.blue) {
            countBlue++;
         }else { // removed if statement instead else without expression used
            countGreen++;
         }
      }

      if (countRed == balls.length) {
         return;
      } else if (countRed == 0) {
         return;
      }

      for (int i = 0; i < countRed; i++) {
         balls[i] = Color.red;
      }

      for (int i = countRed; i < balls.length; i++) {
         balls[i] = Color.green;
      }

      for (int i = countRed+countGreen; i < balls.length; i++) {
         balls[i] = Color.blue;
      }
   }
}
